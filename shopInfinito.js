const SDKutil = require('./SDKutil');
const api = SDKutil.api;
var genWallet = SDKutil.genWallet;
var txParams = SDKutil.txParams;

var shopTransferPoint = (to, pointID, amount, shopPrivateKey) => {
    return new Promise((resolve, reject) => {
        txParams.sc.nameFunc = 'shopTransferPoint';
        txParams.sc.typeParams = ['address', 'uint', 'uint'];
        txParams.sc.paramsFuncs = [to, pointID, amount];
        var wallet = genWallet(shopPrivateKey);
        wallet.setApi(api);
        SDKutil.executeT(txParams, wallet).then((result) => {
            resolve(result)
        }, (error) => {
            reject(error)
        })
    })
}
var createPoint = (_shopAddress, _pointName, _pointSymbol, _pointDecimals, _initialSupply, ownerPrivateKey) => {
    return new Promise((resolve, reject) => {
        txParams.sc.nameFunc = 'createPoint';
        txParams.sc.typeParams = ['address', 'string', 'string', 'uint8', 'uint256'];
        txParams.sc.paramsFuncs = [_shopAddress, _pointName, _pointSymbol, _pointDecimals, _initialSupply];
        var wallet = genWallet(ownerPrivateKey);
        wallet.setApi(api);
        SDKutil.executeT(txParams, wallet).then((result) => {
            resolve(result)
        }, (error) => {
            reject(error)
        })
    })
}
shopTransferPoint("0x32dA8eED08D6ae33167b9afb58fae1fCAE1eb1F2",6, 100, "0xBD2218BEE53EAD25BEE75ADB404CDDC5AD46F0059B07351F605DD49849B0F4ED")
.then(result => {
    console.log(result.tx_id);
    SDKutil.getTxResult(result.tx_id)
        .then(result => {
            console.log(result);
        })
})
var earnMorePoint = (_pointID, _amount, ownerPrivateKey) => {
    return new Promise((resolve, reject) => {
        txParams.sc.nameFunc = 'earnMorePoint';
        txParams.sc.typeParams = ['uint256', 'uint256'];
        txParams.sc.paramsFuncs = [_pointID, _amount];
        var wallet = genWallet(ownerPrivateKey);
        wallet.setApi(api);
        SDKutil.executeT(txParams, wallet).then((result) => {
            resolve(result)
        }, (error) => {
            reject(error)
        })
    })

}


