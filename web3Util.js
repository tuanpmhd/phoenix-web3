const Web3 = require('web3');
const fs = require('fs');
//const ganacheURL = 'http://localhost:7545';

/**
 *  Using webJs to execute 'call method' on smart contract
 */
var initWeb3 = () => {
    //  (1) change the provider: infura or ganache
    var web3Provider = new Web3.providers.HttpProvider(infuraURL);
    web3 = new Web3(web3Provider);
    return web3;
}
/**
 * 
 * @param {object} web3 
 * @param {string} jsonPath 
 * @param {string} address : address of contract to init
 */
var initContract = (web3, jsonPath, address) => {
    var ABIString = fs.readFileSync(jsonPath);
    var ABI = JSON.parse(ABIString).abi;
    return new web3.eth.Contract(ABI, address);

}
const infuraURL = 'https://ropsten.infura.io/v3/9ff087f6232f43998ea7c7c3b3d22e10';
const coinContractRopstenAddress = "0x4729063D74275894231C1467AdCc6FC43392bA84";
var web3 = initWeb3();

var contract = initContract(web3, "./Coin.json", coinContractRopstenAddress);
module.exports = {
    initWeb3,
    initContract,
    contract
}


