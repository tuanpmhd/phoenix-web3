const SDKutil = require('./SDKutil');
const api = SDKutil.api;
var genWallet = SDKutil.genWallet;
var txParams = SDKutil.txParams;

// contract owner call this, require private key
var expiredTrading = (publisher, pointPublisherID, pointPublisherAmount, privateKey) => {
    return new Promise((resolve, reject) => {
        txParams.sc.nameFunc = 'expiredTrading';
        txParams.sc.typeParams = ['address', 'uint256', 'uint256'];
        txParams.sc.paramsFuncs = [publisher, pointPublisherID, pointPublisherAmount];
        var wallet = genWallet(privateKey);
        wallet.setApi(api);
        SDKutil.executeT(txParams, wallet)
        .then((result) => {
            resolve(result)
        }, (error) => {
            reject(error)
        })
    })
}
// <1> customer calling this, require private key
var customerSendPointTrading = (publisher, pointPublisherID, pointTraderID, pointPublisherAmount, pointTraderAmount, privateKey) => {
    return new Promise((resolve, reject) => {
        txParams.sc.nameFunc = 'customerSendPointTrading';
        txParams.sc.typeParams = ['address', 'uint256', 'uint256', 'uint256', 'uint256'];
        txParams.sc.paramsFuncs = [publisher, pointPublisherID, pointTraderID, pointPublisherAmount, pointTraderAmount];
        var wallet = genWallet(privateKey);
        wallet.setApi(api);
        SDKutil.executeT(txParams, wallet).then((result) => {
            resolve(result)
        }, (error) => {
            reject(error)
        })
    })
}

// // <2> trader calling this, require private key
var traderApproveTrading = (trader, publisher, pointPublisherID, pointTraderID, pointPublisherAmount, pointTraderAmount, privateKey) => {
    return new Promise((resolve, reject) => {
        txParams.sc.nameFunc = 'traderApproveTrading';
        txParams.sc.typeParams = ['address', 'address', 'uint256', 'uint256', 'uint256', 'uint256'];
        txParams.sc.paramsFuncs = [trader,publisher, pointPublisherID, pointTraderID, pointPublisherAmount, pointTraderAmount];
        var wallet = genWallet(privateKey);
        wallet.setApi(api);
        SDKutil.executeT(txParams, wallet).then((result) => {
            resolve(result)
        }, (error) => {
            reject(error)
        })
    })
}

// // <3> customer calling this, require private key. 
var customerTransferPointToCustomer = (to, pointID, pointsTransfer, privateKey) => {
    return new Promise((resolve, reject) => {
        txParams.sc.nameFunc = 'customerTransferPointToCustomer';
        txParams.sc.typeParams = ['address', 'uint', 'uint'];
        txParams.sc.paramsFuncs = [to, pointID, pointsTransfer];
        var wallet = genWallet(privateKey);
        wallet.setApi(api);
        SDKutil.executeT(txParams, wallet).then((result) => {
            resolve(result)
        }, (error) => {
            reject(error)
        })
    })
}
customerTransferPointToCustomer("0x086c1268DFea79eaa32DbA2271DeCF685049753D", 6, 69, "0xCB706FED304AD0B0CF8EB019415ED31BA803F8C1E253088DF5AAA56A1AAB96AF")
.then((result) => {
    console.log(result.tx_id);
}, (error) => {
    console.log(error);
})
// // <4> customer calling this, require private key. 
var customerTransferPointToShop = (to, pointID, pointsTransfer, privateKey) => {
    return new Promise((resolve, reject) => {
        txParams.sc.nameFunc = 'customerTransferPointToShop';
        txParams.sc.typeParams = ['address', 'uint', 'uint'];
        txParams.sc.paramsFuncs = [to, pointID, pointsTransfer];
        var wallet = genWallet(privateKey);
        wallet.setApi(api);
        SDKutil.executeT(txParams, wallet).then((result) => {
            resolve(result)
        }, (error) => {
            reject(error)
        })
    })
}
